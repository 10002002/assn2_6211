﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Assignment2_6211
{
    public class Files
    {
        static void Main(string[] args)
        {
            ImportData();
            
            Console.ReadLine();
        }

        public static void ImportData()
        {
            List<string> lines = new List<string>();
            using (StreamReader importData = File.OpenText(@"C:\Users\10002002\Desktop\6211\Assignment2_6211\Assignment2_6211\Moisture_Data.txt"))
            {
                while (!importData.EndOfStream)
                {
                    lines.Add(importData.ReadLine());
                }
            }
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
        }
    }
}